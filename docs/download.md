<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

# Download

These are the released versions of [trivrepr](index.md) available for download.

## [0.2.1] - 2024-10-05

### Source tarball

- [trivrepr-0.2.1.tar.gz](https://devel.ringlet.net/files/devel/trivrepr/trivrepr-0.2.1.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/trivrepr/trivrepr-0.2.1.tar.gz.asc))

### Python wheel

- [trivrepr-0.2.1-py3-none-any.whl](https://devel.ringlet.net/files/devel/trivrepr/trivrepr-0.2.1-py3-none-any.whl)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/trivrepr/trivrepr-0.2.1-py3-none-any.whl.asc))

## [0.2.0] - 2024-10-05

### Source tarball

- [trivrepr-0.2.0.tar.gz](https://devel.ringlet.net/files/devel/trivrepr/trivrepr-0.2.0.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/trivrepr/trivrepr-0.2.0.tar.gz.asc))

### Python wheel

- [trivrepr-0.2.0-py3-none-any.whl](https://devel.ringlet.net/files/devel/trivrepr/trivrepr-0.2.0-py3-none-any.whl)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/trivrepr/trivrepr-0.2.0-py3-none-any.whl.asc))

## [0.1.1] - 2019-03-07

### Source tarball

- [trivrepr-0.1.1.tar.gz](https://devel.ringlet.net/files/devel/trivrepr/trivrepr-0.1.1.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/trivrepr/trivrepr-0.1.1.tar.gz.asc))

### Python wheel

- [trivrepr-0.1.1-py3-none-any.whl](https://devel.ringlet.net/files/devel/trivrepr/trivrepr-0.1.1-py3-none-any.whl)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/trivrepr/trivrepr-0.1.1-py3-none-any.whl.asc))

## [0.1.0] - 2019-03-06

### Source tarball

- [trivrepr-0.1.0.tar.gz](https://devel.ringlet.net/files/devel/trivrepr/trivrepr-0.1.0.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/trivrepr/trivrepr-0.1.0.tar.gz.asc))

### Python wheel

- [trivrepr-0.1.0-py3-none-any.whl](https://devel.ringlet.net/files/devel/trivrepr/trivrepr-0.1.0-py3-none-any.whl)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/trivrepr/trivrepr-0.1.0-py3-none-any.whl.asc))

[0.2.1]: https://gitlab.com/ppentchev/trivrepr/-/tags/0.2.1
[0.2.0]: https://gitlab.com/ppentchev/trivrepr/-/tags/0.2.0
[0.1.1]: https://gitlab.com/ppentchev/trivrepr/-/tags/0.1.1
[0.1.0]: https://gitlab.com/ppentchev/trivrepr/-/tags/0.1.0
