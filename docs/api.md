<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

# API Reference

::: trivrepr.TrivialRepr

::: trivrepr.TrivialReprWithJson
