<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

# Change log for trivrepr

- 0.1.1 (2019-03-07)
  - Add the news.py tool for preparing the distribution changelog.
  - Correct the project's homepage URL.
  - Use setuptools-git to include more files in the sdist tarball.

- 0.1.0 (2019-03-06)
  - Initial public release.
